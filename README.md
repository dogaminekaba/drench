# Description

Click the color buttons to drench all the cells with one color, starting from the top left cell. You have limited amount of moves to achieve this goal!

# Screenshots

![Drench_play](https://bitbucket.org/dogaminekaba/drench/downloads/Drench_play.png)