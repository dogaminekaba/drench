package
{
  import flash.net.URLRequest;

  public class Assets
  {
    public static const redButonUrl:URLRequest = new URLRequest('../Images/red_button.png');
    public static const blueButonUrl:URLRequest = new URLRequest('../Images/blue_button.png');
    public static const yellowButonUrl:URLRequest = new URLRequest('../Images/yellow_button.png');
    public static const greenButonUrl:URLRequest = new URLRequest('../Images/green_button.png');
    public static const orangeButonUrl:URLRequest = new URLRequest('../Images/orange_button.png');
    public static const purpleButonUrl:URLRequest = new URLRequest('../Images/purple_button.png');
  }
}