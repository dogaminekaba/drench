package objects
{
  import flash.display.Sprite;
  import flash.utils.Dictionary;
  
  import utilities.GameProperties;
  import utilities.GameUtilities;
  
  public class Board extends Sprite
  {
    private var size:int;
    private var grids:Array;
    private var myGrids:Array;
    private var map:Dictionary;
    
    public function Board()
    {
      super();
      
      this.x = GameProperties.boardPositionX;
      this.y = GameProperties.boardPositionY;
      size = GameProperties.boardSize;
      grids = new Array();
      myGrids = new Array();
      
      map = new Dictionary();
      fillBoard();
    }
    
    private function fillBoard():void
    {
      var i:int, j:int;
      var square:Square;
      var squareLength:int = GameProperties.squareLength;
      
      for(i=0; i<size; ++i)
      {
        for(j=0; j<size; ++j)
        {
          square = new Square(j*squareLength, i*squareLength, GameUtilities.randomColor(GameProperties.colorCount));
          square.gridX = j;
          square.gridY = i;
          grids.push(square);
          addChild(square);
          map[square.gridX.toString() + '_' + square.gridY.toString()] = square;
        }
      }
      
      grids[1].color = grids[0].color;
      grids[2].color = grids[0].color;
      grids[3].color = grids[0].color;
      
      var ss:Square = grids.shift();
      sameColors(ss,myGrids);
      myGrids.push(ss);
      
      uncheckAll();
    }
    
    public function updateBoard(color:uint):void
    {
      var controlGrids:Array = [];
      var currentNeighbours:Array = [];
      var index:int = 0;
      var i:int;
      var square:Square;
      
      for (i = 0; i < myGrids.length; i++) 
      {
        currentNeighbours = findNeighbours(myGrids[i]);
        
        for (var k:int = 0; k < currentNeighbours.length; k++) 
        {
          index = controlGrids.indexOf(currentNeighbours[k]);
          if (index == -1) 
          {
            index = myGrids.indexOf(currentNeighbours[k]);
            if (index == -1) 
            {
              controlGrids.push(currentNeighbours[k]);
              sameColors(currentNeighbours[k],currentNeighbours);
              uncheckAll();
            }
          }
        }
      }
      
      for (i = 0; i < myGrids.length; i++) 
      {
        myGrids[i].color = color;
      }
      
      for (i = 0; i<controlGrids.length; ++i) 
      {
        if(controlGrids[i].color == color)
        {
          index = grids.indexOf(controlGrids[i]);
          
          square =  grids.splice(index, 1)[0];
          myGrids.push(square);
          
          delete map[square.gridX.toString() + '_' + square.gridY.toString()];
        }
      }
    }
    
    private function findNeighbours(square:Square):Array
    {
      var positions:Array = [{x:0, y:1}, {x:0, y:-1}, {x:-1, y:0}, {x:1, y:0}];
      var result:Array = [];
      var dX:int; 
      var dY:int;
   
      for (var i:int = 0; i < positions.length; i++) 
      {
        dX = square.gridX + positions[i].x;
        dY = square.gridY + positions[i].y;
       
        if(dX >= 0 && dX < size)
        {
          if (dY >= 0 && dY < size)
          {
            var s:Square = map[dX.toString() + '_' + dY.toString()];
            if(s)
            {
              result.push(s);
            }
          }
        }
      }
      return result;
    }
    
    private function uncheckAll():void
    {
      for (var i:int = 0; i < grids.length; i++) 
      {
        grids[i].checked = false;
      }

      for (i = 0; i < myGrids.length; i++) 
      {
        myGrids[i].checked = false;
      }
    }
    
    private function sameColors(square:Square, list:Array):void
    {
      var n:Array = findNeighbours(square);
      square.checked = true;
      for (var i:int = 0; i < n.length; i++) 
      {
        if(n[i].checked) continue;
        
        if(square.color == n[i].color)
        {
          if(list.indexOf(n[i]) == -1)
          {
            list.push(n[i]);
            sameColors(n[i],list);
          }
        }
      }
    }
    
    public function oneColoredSquares():int
    {
      var result:int = 0;
      
      for (var i:int = 0; i < myGrids.length; i++) 
      {
        if(myGrids[0].color == myGrids[i].color)
          ++result;
      }
      return result;
    }
    
  }
}