package objects
{
  import flash.display.Sprite;
  
  import utilities.GameProperties;

  public class Square extends Sprite
  {
    public var gridX:int;
    public var gridY:int;
    public var checked:Boolean;
    
    public function Square(x:int, y:int, color:uint) 
    {
      super();
      
      this.x = x;
      this.y = y;
      this.color = color;
    }
    
    private var _color:uint;
    public function get color():uint { return _color; }
    
    public function set color(value:uint):void
    {
      if (_color == value)
        return;
      _color = value;
      this.graphics.beginFill(_color);
      this.graphics.drawRect(0, 0, GameProperties.squareLength, GameProperties.squareLength);
      this.graphics.endFill();
    }
  }
}