package objects
{
  import flash.display.Loader;
  import flash.display.MovieClip;
  import flash.display.Sprite;
  import flash.events.MouseEvent;
  
  import utilities.GameProperties;
  
  public class Button extends Sprite
  {
    private var color:uint;
    private var buttonImage:MovieClip;
    private var loader:Loader;
    private var loaderInitialX:Number;
    private var loaderInitialY:Number;
    public function Button(x:int, y:int, color:uint)
    {
      super();
      this.x = x;
      this.y = y;
      this.color = color;
      
      loader = new Loader();
      loadButtonImage();
      addChild(loader);
      loaderInitialX = loader.x;
      loaderInitialY = loader.y;
      
      this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
      this.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
      this.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
    }
    
    protected function onMouseDown(event:MouseEvent):void
    {
      //click effect down
      loader.scaleX = 0.8;
      loader.x += 4; 
      loader.scaleY = 0.8;
      loader.y += 4;
    }
    
    protected function onMouseOut(event:MouseEvent):void
    {
      //click effect up
      loader.scaleX = 1;
      loader.x = loaderInitialX; 
      loader.scaleY = 1;
      loader.y = loaderInitialY;
    }
    
    protected function onMouseUp(event:MouseEvent):void
    {
      //click effect up
      loader.scaleX = 1;
      loader.x = loaderInitialX;
      loader.scaleY = 1;
      loader.y = loaderInitialY;
      
      if (!Drench.finished) 
      {
        if(Drench.counter > 0)
        {
          Drench.board.updateBoard(color);
          if(Drench.board.oneColoredSquares() == GameProperties.boardSize*GameProperties.boardSize)
          {
            Drench.counterLabel.text = "YOU WIN!";
            Drench.finished = true;
          }
          else
          {
            Drench.counter -=1;
            if(Drench.counter == 0)
            {
              Drench.counterLabel.text = "YOU LOSE!";
              Drench.finished = true;
            }
            else
            Drench.counterLabel.text = Drench.counter.toString() + " MOVES LEFT!";
          }
        }
      }
      
        
    }
    private function loadButtonImage():void
    {
      switch(color)
      {
        case GameProperties.colorRed:
          loader.load(Assets.redButonUrl);
          break;
        case GameProperties.colorBlue:
          loader.load(Assets.blueButonUrl);
          break;
        case GameProperties.colorYellow:
          loader.load(Assets.yellowButonUrl);
          break;
        case GameProperties.colorGreen:
          loader.load(Assets.greenButonUrl);
          break;
        case GameProperties.colorOrange:
          loader.load(Assets.orangeButonUrl);
          break;
        case GameProperties.colorPurple:
          loader.load(Assets.purpleButonUrl);
          break;
        default:
          break;
      }
    }
  }
}