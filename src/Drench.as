package
{
  import flash.display.Sprite;
  import flash.events.Event;
  import flash.events.MouseEvent;
  import flash.text.TextField;
  import flash.text.TextFieldAutoSize;
  import flash.text.TextFormat;
  
  import objects.Board;
  import objects.Button;
  
  import utilities.GameProperties;
  
  [SWF(frameRate="60", width="800", height="600", backgroundColor="0xbbddff")]
  public class Drench extends Sprite
  {
    public static var counter:int;
    public static var board:Board;
    private var button:Button;
    public static var counterLabel:TextField = new TextField();
    public static var finished:Boolean;
    public function Drench()
    {
      this.addEventListener(Event.ADDED_TO_STAGE,onAddedtoStage);
    }
    
    protected function onAddedtoStage(event:Event):void
    {
      this.removeEventListener(Event.ADDED_TO_STAGE,onAddedtoStage);
      stage.addEventListener(MouseEvent.RIGHT_CLICK, onRightClick);
      
      finished = false;
      
      counter = GameProperties.moveCount;
      createCounter();
      counterLabel.text = counter.toString() + " MOVES LEFT!";
      
      board = new Board();
      addChild(board);
      
      addButtons();
      
    }
    
    protected function onRightClick(event:MouseEvent):void
    {
      //intentionally left blank
    }
    
    private function addButtons():void
    {
      button = new Button(GameProperties.redButtonX, GameProperties.redButtonY, GameProperties.colorRed);
      addChild(button);
      
      button = new Button(GameProperties.blueButtonX, GameProperties.blueButtonY, GameProperties.colorBlue);
      addChild(button);
      
      button = new Button(GameProperties.yellowButtonX, GameProperties.yellowButtonY, GameProperties.colorYellow);
      addChild(button);
      
      button = new Button(GameProperties.greenButtonX, GameProperties.greenButtonY, GameProperties.colorGreen);
      addChild(button);
      
      button = new Button(GameProperties.orangeButtonX, GameProperties.orangeButtonY, GameProperties.colorOrange);
      addChild(button);
      
      button = new Button(GameProperties.purpleButtonX, GameProperties.purpleButtonY, GameProperties.colorPurple);
      addChild(button);
    }
    
    private function createCounter():void
    {
      counterLabel = new TextField();
      counterLabel.autoSize = TextFieldAutoSize.CENTER;
      
      var format:TextFormat = new TextFormat();
      format.font = "Verdana";
      format.color = 0x00aaFF;
      format.size = 50;
      
      counterLabel.x = GameProperties.counterX;
      counterLabel.y = GameProperties.counterY;
      
      counterLabel.defaultTextFormat = format;
      addChild(counterLabel);
    }
  }
}