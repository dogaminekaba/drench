package utilities
{
  public class GameProperties
  {
    public static const colorCount:int = 6;
    
    public static const squareLength:int = 20;
    
    public static const boardPositionX:int = 180;
    public static const boardPositionY:int = 140;
    
    public static const boardSize:int = 14;
    
    public static const moveCount:int = 30;
    
    public static const counterX:int = 400;
    public static const counterY:int = 20;
    
    public static const colorRed:uint = 0xFE2E2E;
    public static const colorBlue:uint = 0x16A6DB;
    public static const colorYellow:uint = 0xF4FA58;
    public static const colorGreen:uint = 0x55D719;
    public static const colorOrange:uint = 0xFE9A2E;
    public static const colorPurple:uint = 0x9F81F7;
    
    public static const redButtonX:int = 510;
    public static const redButtonY:int = 200;
    public static const blueButtonX:int = 510;
    public static const blueButtonY:int = 260;
    public static const yellowButtonX:int = 510;
    public static const yellowButtonY:int = 320;
    
    public static const greenButtonX:int = 570;
    public static const greenButtonY:int = 200;
    public static const orangeButtonX:int = 570;
    public static const orangeButtonY:int = 260;
    public static const purpleButtonX:int = 570;
    public static const purpleButtonY:int = 320;
    
  }
}