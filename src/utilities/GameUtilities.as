package utilities
{
  
  public class GameUtilities
  {
    
    public static function randomColor(colorCount:int):uint
    {
      var colorType:int = randomInt(0, colorCount-1);
      
      switch(colorType)
      {
        case 0:
          return GameProperties.colorRed;
          break;
        case 1:
          return GameProperties.colorBlue;
          break;
        case 2:
          return GameProperties.colorYellow;
          break;
        case 3:
          return GameProperties.colorGreen;
          break;
        case 4:
          return GameProperties.colorOrange;
          break;
        case 5:
          return GameProperties.colorPurple;
        default:
          return 0xffffff;
          break;
      }
    }
    
    public static function randomInt(minNum:int, maxNum:int):int 
    {
      return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
    }
  }
}